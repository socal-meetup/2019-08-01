# First Ever Orange County GitLab Users Group

## 2019-08-01
### Let's bring the GitLab community together to discuss best practices, tips, and tricks. Let's collaborate with each other to build up the DevSecOps skill sets and methodologies.


Location: 114 Pacifica, Irvine, CA, 92618 (Suite 290)

URL: https://www.meetup.com/Orange-County-GitLab-Users-Group/events/263201035/

__Topics:__
*  Q&A
*  Example Projects
*  Best Practices
*  War Stories

__Submitted Discussion:__
*  Gitlab global availability, advanced scheduling, k8s integration direction, PaaS and generally how to make everything wonderful.
*  How orgs change to adopt DevOps.
*  Best Practices - Data Science, Change Control.
*  Advanced CI Topics, When can you not use pipelines? Structure of projects
- https://docs.gitlab.com/ee/ci/yaml/#onlychangesexceptchanges
- For Anton:  Use the only changes:except clause to get multi pipeline workflows.  
*  Release Info
*  Pipeline File? 
*  Docker for windows runner
*  

SAFe - https://youtu.be/PmFFlTH2DQk